# gwlab_cwfollowup_helm

Helm chart repository for [gwlab_cwfollowup](https://gitlab.com/gwdc/gwlab_cwfollowup/)

## Assumptions
- Helm3 is [installed](https://helm.sh/docs/intro/install/)
- Relevant docker images and container image registry exists
- Relevant vault access and secrets exists
- Relevant storage access and storage solutions exist

## Repo Initialisation
- Initialise helm repo by executing `helm create gwlab_cwfollowup`
- Move helm output by `mv gwlab_cwfollowup/* . && mv gwlab_cwfollowup/.helmignore .`
- Cleanup `rmdir gwlab_cwfollowup`

## Chart components
| K8s Resource | Comment |
| --- | --- |
| django_deployment | Stateful deployment for gwlab_cwfollowup application. |
| django_service | k8s construct allowing inter micro service communication |
| nginx_deployment | deployment for nginx internal proxy |
| nginx_service | k8s construct allowing inter micro service communication |
| static_deployment | deployment involving static files |
| static_service | k8s construct allowing inter micro service communication |
| role | Access rules to k8s resources |
| rolebinding | k8s construct to assigning role(s) to service account(s) |
| secrets | Placeholder resource populated by init container defined in `django_deployment` |
| serviceaccount | k8s application account used to convert secrets stored in Hashicorp Vault to K8s secrets through an init container defined in `django_deployment` |
|||

## Chart values
Dynamic variables declared and initialised in [values.yaml](./values.yaml)
| Variable | Default | Comment |
| --- | --- | --- |
| django.image.repository | `nexus.gwdc.org.au/docker/gwlab_cwfollowup_django` | |
| django.image.tag | `""` |  Main chart application `gwlab-cwfollowup-django` image tag controlled through `appVersion` in [Chart.yaml](./Chart.yaml) |
| nginx.image.repository | `nexus.gwdc.org.au/docker/gwlab_cwfollowup_nginx` | |
| nginx.image.tag | `0.1` | |
| static.image.repository | `nexus.gwdc.org.au/docker/gwlab_cwfollowup_static` | |
| static.image.tag | `0.35` | |
|||

## Architecture
TBA

## Prerequisites
```bash
# Vault dependencies
#   Vault cli required
#   cli must have network access to vault
vault login -address=$VAULT_HTTPS_FQDN -method=github -token=$ACCESS_TOKEN

# Add vault policy
tee cwfollowup.policy.hcl <<EOF
# Read bilby deployment config
path "kv/gwcloud/cwfollowup"
{
  capabilities = ["list", "read"]
}

# Read gwcloud common credentials
path "kv/gwcloud/common"
{
  capabilities = ["list", "read"]
}
EOF
vault policy write cwfollowup cwfollowup.policy.hcl

# Sanity Check
vault policy read bilby

# Add vault kubernetes role
vault write auth/kubernetes/role/gwlab-cwfollowup \
    bound_service_account_names=gwlab-cwfollowup \
    bound_service_account_namespaces=gwcloud \
    policies=default,cwfollowup \
    ttl=1h

# Sanity Check
vault read auth/kubernetes/role/gwlab-cwfollowup
```

## Chart Development
```bash
# Code Linting
helm lint

# Display default helm values
helm show values .

# Render template files with values.yaml as payload
helm template .

# Packaging
#   Chart packaged as $(Chart.name)-$(Chart.version).tgz
rm -rf *.tgz
helm package $(PWD)

# Nexus upload
curl -u "$(NEXUS_ID):$(NEXUS_PASSWORD)" https://nexus.gwdc.org.au/repository/helm/ --upload-file `ls *.tgz` -v
```

## Support
TBA

## ToDo
- [x] Integrate Vault secrets.
- [x] Proof of concept deployment.
- [x] CI for testing the helm chart.
- [ ] CD for deploying packaged charts to `https://nexus.gwdc.org.au/#browse/browse:helm`
- [ ] CD Runtime test to dev k8s cluster.